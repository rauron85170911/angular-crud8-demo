import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../../modelo/usuario.model';
import { ApiService } from '../../service/api.service';

@Component({
  selector: 'app-listar-usuario',
  templateUrl: './listar-usuario.component.html',
  styleUrls: ['./listar-usuario.component.scss']
})
export class ListarUsuarioComponent implements OnInit {

Usuario: any = [];

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    if (!window.localStorage.getItem('token')) {
      /*this.router.navigate(['login']);
      return;*/
    }
    this.apiService.getUsers().subscribe( data => {
      this.Usuario = data;
    });

  }

  eliminarUsuario(usuario: Usuario): void {
    this.apiService.deleteUser(usuario.id).subscribe(data => {
      this.Usuario = this.Usuario.filter( u  => u !== usuario);
    });
  }
  editarUsuario(): void {
    /* Modificar cuando se habilite el JWT */
    /*window.localStorage.removeItem('editUserId');
    window.localStorage.setItem('editUserid', usuario.id.toString());*/

    /*this.router.navigate(['editar-usuario' + this.Usuario.id]);*/
  }

  anadirUsuario(): void {
    this.router.navigate(['anadir-usuario']);
  }

}
