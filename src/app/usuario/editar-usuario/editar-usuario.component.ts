import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Usuario } from '../../modelo/usuario.model';
import { ApiService } from '../../service/api.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.scss']
})
export class EditarUsuarioComponent implements OnInit {

  id: number;
  usuario: Usuario;
  editForm: FormGroup;
  /*usuarioData: any = {};*/
  /*apiResponse: any;*/
  data: Usuario;
  constructor(private formBuilder: FormBuilder, public router: Router, private apiService: ApiService, public actRoute: ActivatedRoute) {
    this.data = new Usuario();
  }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id;
    /*let userId: any = window.localStorage.getItem('token');*/
    /*if (!userId) {
      /* HAY QUE REVISAR COMO FUNCIONA JWT */
      /*alert('accion erronea');
      this.router.navigate(['listar-usuario']);
      return;
    }*/
    this.editForm = this.formBuilder.group({
      id: [''],
      username: ['', Validators.required],
      password: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      age: ['', Validators.required],
      salary: ['', Validators.required],
    });
    this.apiService.getUserById(this.id).subscribe(response => {
      console.log(response);
      this.data = response;
    });
  }
  onSubmit() {
    this.apiService.updateUser(this.id, this.data)
    .pipe(first())
    .subscribe(
      response => {
        this.router.navigate(['listar-usuario']);
        alert(`${this.id}`);
        alert(`${JSON.stringify(data)}`);
        alert(`${JSON.stringify(this.editForm.value)}`);
        /*if (data.status === 200) {
          alert('Usuario creado con exito');
          this.router.navigate(['listar-usuario']);
        } else {
          alert(data.message);
        }*/
      },
      error => {
        alert(error);
      });

      /*this.apiService.updateUser(this.id, this.editForm.value).subscribe(data => {
        alert(`${this.id}`);
        alert(`${JSON.stringify(data)}`);
        alert(`${JSON.stringify(this.editForm.value)}`);
        this.router.navigate(['listar-usuario']);
      });*/
  }

}
