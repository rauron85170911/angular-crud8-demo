import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuario } from '../modelo/usuario.model';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ApiResponse } from '../modelo/api.response';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  // define api
  apiUrl = 'http://localhost:3000'; /***  /usuarios ***/

  // Http options
  httpOptions = {
    header: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  login(loginPayLoad): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`http://localhost:3000/token/generate-token`, JSON.stringify(loginPayLoad));
  }

  getUsers(): Observable<Usuario> {
    return this.http.get<Usuario>(this.apiUrl + `/usuarios`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getUserById(id: number): Observable<Usuario> {
    return this.http.get<Usuario>(this.apiUrl + `/usuarios/${id}`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  createUser(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.apiUrl + `/usuarios`, usuario);
  }

  updateUser(usuario: Usuario) {
    return this.http.put(this.apiUrl + `/usuarios/${usuario.id}`, JSON.stringify(usuario))
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  deleteUser(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.apiUrl + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // Error handling
  handleError(error) {
     let errorMessage = '';
     if (error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Codigo Error: ${error.status}\nMensaje: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

}
