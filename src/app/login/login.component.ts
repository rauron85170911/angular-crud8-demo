import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../service/api.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService) {}

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    const loginPayLoad = {
      username: this.loginForm.controls.username.value,
      passwors: this.loginForm.controls.password.value,
    };
    this.apiService.login(loginPayLoad).subscribe(data => {
      debugger;
      if (data.status === 100) {
        window.localStorage.setItem('token', data.result.token);
        this.router.navigate(['listar-usuario']);
      } else {
        this.invalidLogin = true;
        alert(data.message);
      }
    });
  }

  ngOnInit() {
    window.localStorage.removeItem('token');
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]
    });
  }

}
