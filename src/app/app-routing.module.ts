import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ListarUsuarioComponent } from './usuario/listar-usuario/listar-usuario.component';
import { AnadirUsuarioComponent } from './usuario/anadir-usuario/anadir-usuario.component';
import { EditarUsuarioComponent } from './usuario/editar-usuario/editar-usuario.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'anadir-usuario', component: AnadirUsuarioComponent},
  {path: 'listar-usuario', component: ListarUsuarioComponent},
  {path: 'editar-usuario/:id', component: EditarUsuarioComponent},
  {path: '', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
